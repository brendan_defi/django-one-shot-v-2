from django.forms import ModelForm
from todos.models import TodoList, TodoItem


# create form class for TodoList
class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


# create form class for TodoItem
class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = [
            "task",
            "due_date",
            "is_completed",
            "list",
        ]
