from django.contrib import admin
from todos.models import TodoList, TodoItem


# register TodoList Model
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "id",
    ]

# alternative (inferior) method
# admin.site.register(TodoList)


# register TodoItem Model
@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
